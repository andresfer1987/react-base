import React from "react";
import { render, fireEvent, getByTestId } from "@testing-library/react";
import { MemoryRouter } from "react-router-dom";

import { Store } from "global/Store";
import Home from "pages/Home";

describe("test home page", () => {
  const dispatch = jest.fn();
  const state: any = {};

  it("should render main childs components", () => {
    const { getByTestId } = render(
      <MemoryRouter initialEntries={["/"]} initialIndex={0}>
        <Store.Provider
          value={{
            state,
            dispatch
          }}
        >
          <Home />
        </Store.Provider>
      </MemoryRouter>
    );

    expect(getByTestId("HomeComponent")).toBeTruthy();
  });
});
